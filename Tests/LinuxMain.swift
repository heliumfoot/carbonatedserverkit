import XCTest
@testable import CarbonatedServerKitTests

XCTMain([
    testCase(CarbonatedServerKitTests.allTests),
])
